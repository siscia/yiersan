(ns yiersan.backend)

(defn check-info-stripe [e]
  e)

(defn get-payment [e]
  e)

(defn save-user [e]
  e)

(defn validate-card-number [n]
  (.validateCardNumber (.-card js/Stripe) n))

(defn validate-expire [month year]
  (.validateExpiry (.-card js/Stripe) month year))

(defn validate-cvc [cvc]
  (.validateCVC (.-card js/Stripe) cvc))

(defn initialize-userapp []
  (.log js/console "initialize")
  (.initialize js/UserApp (js-obj "appId" "5567285387a1a"))
  (.log js/console js/UserApp))

(defn new-user [email password payment callback]
  (.save (.-User js/UserApp) {:email email
                              :login email
                              :password password
                              :properties {"payment" payment}}
         callback))

(defn login [email password callback]
  (.login (.-User js/UserApp) (js-obj "login" email
                                      "password" password)
          callback))
