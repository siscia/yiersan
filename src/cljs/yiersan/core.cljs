(ns yiersan.core
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]
              [secretary.core :as secretary :include-macros true]
              [goog.events :as events]
              [goog.history.EventType :as EventType]
              [ajax.core :refer [GET]]
              [cognitect.transit :as t]
              [yiersan.backend :as b])
    (:import goog.History))

(def reader (t/reader :json))

(defn select-by-id [id]
  (.getElementById js/document id))

(def state
  (atom {:query ""
         :answers []
         :show-login false
         :token ""}))

(defn show-login []
  (swap! state
         #(assoc % :show-login true)))

(defn get-answer [answers]
  (repeat 10 {:title (get answers "aaa")
              :link "aaa"
              :describtion "aaa"}))

(defn add-answer [answers]
  (swap! state assoc :answers answers))

(defn callback-login [error result]
  (if error
    (.log js/console error result)
    (do
      (swap! state assoc :token (get result "token"))
      (set! (.-location js/window) "#/search"))))

(defn callback-register [error result]
  (if error
    (.log js/console error result)
    (do
      (swap! state assoc :token (get result "token")))))

(defn query->answers [query]
  (GET "/search" {:params {:q (str query) :type "Web"}
               :handler (fn [r]
                          (.log js/console "1: " r)
                          (let [r (t/read reader r)]
                            (.log js/console "2: "
                                  (-> r (get "d")
                                      (get "results")))
                            (reset! state {:query query
                                           :answers (-> r
                                                        (get "d")
                                                        (get "results"))})))
               :error-handler (fn [e]
                                (reset! state {:answers [{"Title" "error"}]}))}))

;; -------------------------
;; Views
(defn intro []
  [:div {:style {:margin "2em auto"
                 :max-width "800px"
                 :padding "6rem"
                 :text "center"}}
   [:h1 "QiJo, the search engine without compromise, nor ads."]
   [:div.row
    [:div.columns.two
     [:a {:href "/#/register"} [:button "Register"]]]
    [:div.columns.nine.u-pull-right
     [:form {:on-submit (fn [_]
                          (.log js/console "try login")
                          (let [email (select-by-id "email-login")
                                password (select-by-id "pass-login")]
                            (b/login (.-value email)
                                     (.-value password) callback-login)))}
      [:div.row
       [:div.columns.five [:input#email-login
                           {:type :text
                            :placeholder "Email"}]]
       [:div.columns.five [:input#pass-login
                           {:type :password
                            :placeholder "Password"}]]
       [:div.columns.two [:button "LogIn"]]]]]
    [:h2.column.eleven "What is QiJo ?"]]
   [:p "QiJo is a search engine, just like Google, Bing or DuckDuckGo but we have a different phisolophy."]
   [:h2.column.eleven "In what you believe ?"]
   [:p "We believe in privacy and we believe that you should not been followed by advertiser when you are online."]
   [:h2.column.eleven "What you offer ?"]
   [:p "We offer a new model of online search, we don't sell your data to anybody, you won't be the product."]
   [:h2.column.eleven "Will you track me ?"]
   [:p "We will not track you by default, however if you ask to be tracked, we will do as you wish. You are in charge."]
   [:h2.column.eleven "Why should I want to be tracked ?"]
   [:p "Tracking what link you have visited will help us to provide more relevant result in your search, you may want it, you may don't want it. The important thing is that you are the one in charge."]
   [:h2.column.eleven "How I am sure that you are not selling my data ?"]
   [:div
    [:p "It is important to remember that, other than email and hashed password, we do not have any of your data, unless you expliciting ask us to track you."]
    [:p "You are sure that we will never sell your data because two important factor: "]
    [:ul
     [:li "Reputation: We build this company on trust, we will lost al our customer trust and with that our competitive advantage."]
     [:li "Terms and Condition: We are legally liable if we sell your data."]]]
   [:h2.column.eleven "If I am not the product I am a customer, it means that I need to pay ?"]
   [:p "We are asking you to pay to use our services, on return you get your privacy back, how much is worth your privacy ?"]
   [:h2.column.eleven "I value my privacy a lot, how much your service cost ?"]
   [:p "Our service are cheap, we ask for 10 €/month or 100€/year"]
   [:h2.column.eleven "That is cheaper of what I am expecting, can I start now ?"]
   [:p "Sure, you can" [:a {:hreft "#/register"} " register "] "and start using QiJo immediately."]
   [:h2.column.eleven "What if I don't like the service ?"]
   [:p "We charge you after 5 days, if you don't like our service you can simply drop your subscription."]
   [:h2.column.eleven "Where I register ?"]
   [:p "You can register " [:a {:href "#/register"} "here."]]
   [:h2.column.eleven "I have more questions."]
   [:p "You can contact us at: " [:a {:href "mailto:simone@mweb.biz"} "info@qijo.com"]]])

(def error-register (atom {:card-numer false
                           :cvc false
                           :expire false}))

(defn register-page []
  [:div {:style {:margin "2em"}}
   [:div
    [:h1 "QiJo Registration Form"]]
   [:form {:on-submit (fn [_]
                        (let [card (select-by-id "card-number")
                              cvc (select-by-id "cvc")
                              month (select-by-id "month")
                              year (select-by-id "year")
                              email (select-by-id "email")
                              password (select-by-id "password")]
                          (when-not (b/validate-card-number (-> card .-value))
                            (swap! error-register
                                   assoc :card-number true))
                          (when-not (b/validate-cvc (-> cvc .-value))
                            (swap! error-register
                                   assoc :cvc true))
                          (when-not (b/validate-expire (-> month .-value)
                                                       (-> year .-value))
                            (swap! error-register
                                   assoc :expire true))
                          (b/new-user email password true #(.log js/console "error"))))}
    [:div.row
     [:div.columns.four
      [:h3 "Personal Information"]
      [:label "Your Email"]
      [:input#email.u-full-width {:type :email
                            :required true
                            :placeholder "you @ email . com"
                            :value "aaa@aaa.com"}]
      [:label "Your Password"]
      [:input#password.u-full-width {:type :password
                            :required true
                            :placeholder "Password"
                            :value "hrge"}]
      [:p "Please Read our " [:a {:href "#/terms" :target "_blank"}"Terms of Service"]]]
     [:div.columns.six
      [:h3 "Credit Card Information"]
      [:div.row
       [:div.columns.three [:label "Card Number"]]
       [:div.columns.six [:input#card-number.u-full-width
                          {:type :text
                           :class (when (:card-number @error-register)
                                    "error")
                           :required true
                           :placeholder "Card Number"}]]]
      [:div.row 
       [:div.columns.three [:label "Expiration Date"]]
       [:div.columns.two [:select#month.u-full-width
                          {:class (when (:expire @error-register)
                                    "error")}
                          (for [i (range 1 13)]
                            [:option {:value i} (if (< i 10)
                                                  (str "0" i)
                                                  (str i))])]]
       [:div.columns.one [:label "/"]]
       [:div.columns.three [:select#year.u-full-width
                            {:class (when (:expire @error-register)
                                    "error")}
                            (for [i (range 2015 2021)]
                              [:option {:value i} (str i)])]]]
      [:div.row
       [:div.columns.three [:label "CVC"]]
       [:div.columns.two [:input#cvc.u-full-width
                          {:type :text
                           :class (when (:cvc @error-register) "error")
                           :required true
                           :placeholder "CVC"}]]]
      [:p "You will enjoy five days of QiJo for free, if you don't like our service simply dropout and you will not be charged."]
      [:div.row
       [:div.columns.six
        [:div [:label "I want to pay every month 10€"]]
        [:div [:button.u-full-width "Register and Pay 10€ a month"]]]
       [:div.columns.six
        [:div [:label "I want to pay once a year 100€"]]
        [:div [:button.u-full-width "Register and Pay 100€ a year"]]]]]]]])

(defn home-page []
  [:div [:h2 "Welcome to QiJo"]
   [:div [:a {:href "#/about"} "go to about page"]]])

(defn about-page []
  [:div [:h2 "About QiJo"]
   [:div [:a {:href "#/"} "go to the home page"]]]
  (intro))

(defn current-page []
  [:div [(session/get :current-page)]])

(defn stripe []
  [:div
   [:h2 "Payment"]
   [:form {:action "/charge" :method "POST"} 
    [:script.stripe-button
     {:src "https://checkout.stripe.com/checkout.js"
      :data-key "pk_test_6pRNASCoBOKtIshFeQd4XMUh"
      :data-name "Stripe.com"
      :data-description "2 widgets"
      :data-amount "2000"}]]])

(defn search []
  [:div {:style {:margin "3em"}}
   [:h1 "QiJo"]
   [:form {:on-submit #(query->answers (-> % .-target .-query .-value))}
    [:div.row
     [:div.columns.eight
      [:input.u-full-width {:type :text
                            :id :query
                            :style {:min-height "5rem"}}]]
     [:div.columns.four
      [:input.u-full-width {:type :submit
                            :value "Search"
                            :style {:min-height "5rem"}}]]]]
   (for [el (-> @state :answers)]
     [:div.row
      [:div.columns.nine
       [:a {:href (get el "Url") :target "_blank"}
        [:h3 {:style {:margin-top "0.4em"
                      :margin-bottom "0.2rem"}}
         (get el "Title")]
        [:p {:style {:margin-bottom "0.4em"}}
         (get el "Description")]]]])])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (session/put! :current-page #'home-page))

(secretary/defroute "/about" []
  (session/put! :current-page #'about-page))

(secretary/defroute "/register" []
  (session/put! :current-page #'register-page))

(secretary/defroute "/stripe" []
  (session/put! :current-page #'stripe))

(secretary/defroute "/search" []
  (session/put! :current-page #'search))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (hook-browser-navigation!)
  (mount-root)
  (b/initialize-userapp))
