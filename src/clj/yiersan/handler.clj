(ns yiersan.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [hiccup.core :refer [html]]
            [hiccup.page :refer [include-js include-css]]
            [prone.middleware :refer [wrap-exceptions]]
            [environ.core :refer [env]]
            [org.httpkit.client :as http]))

(def home-page
  (html
   [:html
    [:head
     [:meta {:charset "utf-8"}]
     [:meta {:name "viewport"
             :content "width=device-width, initial-scale=1"}]
     (include-js "https://app.userapp.io/js/userapp.client.js")
     (include-js "https://js.stripe.com/v2/")
     (include-css "css/Skeleton-2.0.4/css/normalize.css")
     (include-css "css/Skeleton-2.0.4/css/skeleton.css")
     (include-css (if (env :dev) "css/site.css" "css/site.min.css"))]
    [:body
     [:div#app]
     (include-js "js/app.js")]]))

(def url-api "https://api.datamarket.azure.com/Bing/Search/v1/")

(defroutes routes
  (GET "/aaa" {params :params} {:status 200
                                :body "{\"d\":{\"results\" : [{\"__metadata\":{\"uri\":\"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\\u0027xbox\\u0027&$skip=14&$top=1\",\"type\":\"WebResult\"},\"ID\":\"07462919-8b1e-4cb0-ad4b-bac514ad08e4\",\"Title\":\"【Xbox/360频道】Xbox/360游戏下载-中关村游戏网\",\"Description\":\"Xbox/360最新最全的Xbox/360游戏、Xbox/360游戏下载、Xbox/360游戏资讯、Xbox/360游戏攻略、Xbox/360游戏图片,Xbox/360游戏下载，更多精彩就在中关村游戏网。\",\"DisplayUrl\":\"xbox.zol.com.cn\",\"Url\":\"http://xbox.zol.com.cn/\"}]}}"})
  (GET "/" [] home-page)
  (GET "/search" {p :params}
       (println p)
       (println (class (:q p)))
       (let [type (:type p)
             url (str url-api type)
             req (http/get url {:query-params {"Query" (str "'" (:q p) "'")
                                              "$format" "JSON"}
                                :basic-auth ["YWmCAZTonRefk6nRQwIirVoXIieE3603wXf/uonRi0w" "YWmCAZTonRefk6nRQwIirVoXIieE3603wXf/uonRi0w"]})]
         (if (< (int (:status @req)) 400)
           {:status 200
            :body (:body @req)}
           {:status 300})))
  (resources "/")
  (not-found "Not Found"))

(def app
  (let [handler (wrap-defaults routes site-defaults)]
    (if (env :dev) (wrap-exceptions handler) handler)))
